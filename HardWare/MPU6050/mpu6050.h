#ifndef __MPU6050_H
#define __MPU6050_H
#include "stm32f4xx.h"
#include "iic.h"

#define MPU6050_Address       0xD0 //宏定义MPU6050从机地址
#define	MPU6050_SMPLRT_DIV		0x19 //
#define	MPU6050_CONFIG			  0x1A
#define	MPU6050_GYRO_CONFIG		0x1B
#define	MPU6050_ACCEL_CONFIG	0x1C

#define	MPU6050_ACCEL_XOUT_H	0x3B
#define	MPU6050_ACCEL_XOUT_L	0x3C
#define	MPU6050_ACCEL_YOUT_H	0x3D
#define	MPU6050_ACCEL_YOUT_L	0x3E
#define	MPU6050_ACCEL_ZOUT_H	0x3F
#define	MPU6050_ACCEL_ZOUT_L	0x40
#define	MPU6050_TEMP_OUT_H		0x41
#define	MPU6050_TEMP_OUT_L		0x42
#define	MPU6050_GYRO_XOUT_H		0x43
#define	MPU6050_GYRO_XOUT_L		0x44
#define	MPU6050_GYRO_YOUT_H		0x45
#define	MPU6050_GYRO_YOUT_L		0x46
#define	MPU6050_GYRO_ZOUT_H		0x47
#define	MPU6050_GYRO_ZOUT_L		0x48

#define	MPU6050_PWR_MGMT_1		0x6B //MPU6050的电源管理寄存器1
#define	MPU6050_PWR_MGMT_2		0x6C //MPU6050的电源管理寄存器2
#define	MPU6050_WHO_AM_I		  0x75 //MPU6050的ID号

void MPU6050_Init(void);
void Write_MPU6050_Register(uint8_t RegisterAddress ,uint8_t Data);
uint8_t Read_MPU6050_Register(uint8_t RegisterAddress);
void Get_MPU6050_Data(int16_t *AccX ,int16_t *AccY ,int16_t *AccZ ,
	                    int16_t *GyroX ,int16_t *GyroY ,int16_t *GyroZ);
#endif
