#include "led.h"
/*
*函数功能：LED相关的GPIO初始化
*传入参数：无
*返回参数：无
*/
void LED_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOF, &GPIO_InitStruct);
    GPIO_WriteBit(GPIOF, GPIO_Pin_9, Bit_SET);
    GPIO_WriteBit(GPIOF, GPIO_Pin_10, Bit_SET);
}

