# STM32F407ZGT6
## 文档说明

此仓库为STM32F4的练习项目，包括一些基本的模块，如LED，OLED，mpu6050，舵机,电机控制等。

项目中部分代码是根据B站博主（江科大自化协）的教程编写，在此感谢大佬的贡献。

[江科大STM32教程](https://www.bilibili.com/video/BV1th411z7sn/?spm_id_from=333.999.0.0)

